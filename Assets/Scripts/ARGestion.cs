using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARGestion : MonoBehaviour
{
    private GameObject ninjaPrefab;
    private GameObject zombiePrefab;
    private Dictionary<string, GameObject> spawnedPrefabs = new Dictionary<string, GameObject>();
    private Animator ninjaAnimator;
    private Animator zombieAnimator;
    private AudioSource audioSource;
    private bool toggleAudio;
    private float distance;
    private float modifyScale;
    private float minScale = 0.04f;
    private float maxScale = 0.2f;
    private float zoomValue = 0.05f;

    public Vector2 startPosition { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        FindPrefabs();
        audioSource = GetComponent<AudioSource>();
        toggleAudio = false;
    }

    // Update is called once per frame
    void Update()
    {
        FindPrefabs();
        distance = DistanceBetweenPrefabs();
        if (distance < 0.2f && distance != 0f)
        {
            ninjaAnimator.SetBool("activeAnim", true);
            zombieAnimator.SetBool("activeAnim", true);

            if(toggleAudio == false)
            {
                audioSource.Play();
                toggleAudio = true;
            }

        } else
        {
            ninjaAnimator.StopPlayback();
            zombieAnimator.StopPlayback();
            ninjaAnimator.SetBool("activeAnim", false);
            zombieAnimator.SetBool("activeAnim", false);

            audioSource.Stop();
            toggleAudio = false;
        }

        // Scale management
        changeScalePrefabs();
    }
    private void FindPrefabs()
    {
        if (ninjaPrefab == null)
        {
            ninjaPrefab = GameObject.FindGameObjectWithTag("ninja");
            ninjaAnimator = ninjaPrefab.GetComponent<Animator>();
        }
        if (zombiePrefab == null)
        {
            zombiePrefab = GameObject.FindGameObjectWithTag("zombie");
            zombieAnimator = zombiePrefab.GetComponent<Animator>();
        }
    }

    private float DistanceBetweenPrefabs()
    {
        return Vector3.Distance(ninjaPrefab.transform.position, zombiePrefab.transform.position);
    }

    private void changeScalePrefabs()
    {
        Touch[] touches = Input.touches;
        if (touches.Length >= 1)
        {
            Touch currentTouch = touches[0];
            if (currentTouch.phase == TouchPhase.Began)
            {
                startPosition = currentTouch.position;
            }
            else if (currentTouch.phase == TouchPhase.Moved)
            {
                if(ScalePrefabs(startPosition, currentTouch.position))
                {
                    float scalingValue = Time.deltaTime* modifyScale;

                    if (scalingValue > minScale && scalingValue < maxScale)
                    {
                        zombiePrefab.transform.localScale = new Vector3(scalingValue, scalingValue, scalingValue);
                        ninjaPrefab.transform.localScale = new Vector3(scalingValue, scalingValue, scalingValue);
                    }
                }
            }
        }
    }

    private bool ScalePrefabs(Vector2 startPosition, Vector2 currentPosition)
    {
        float resultDifference = currentPosition.y - startPosition.y;

        bool isUpwardSwipe = resultDifference > 0;
        bool isDownwardSwipe = resultDifference < 0;

        if (isUpwardSwipe)
        {
            modifyScale += zoomValue;
            return true;
        }
        else if (isDownwardSwipe)
        {
            modifyScale -= zoomValue;
            return true;
        }

        return false;
    }
}
